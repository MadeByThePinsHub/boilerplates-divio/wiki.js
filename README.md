# Divio/Railway Deploy template for Wiki.js

This repo is an Divio app definition and Railway starter for Wiki.js. If there's a issue, [file a new issue in GitLab](https://gitlab.com/RecapTime/infras/wiki.js/-/issues/new).

For information about Wiki.js, check out the following links:

- [Official Website](https://js.wiki)
- [GitHub Repository](https://github.com/Requarks/wiki)

## Getting Started

Forking this repository is recommended so you can experiment with the GitLab CI as needed.

### With Railway

Depending on usage, your monthly bill with your Wiki.js instance on Railway is between US$1.66 to US$2.60, your mileage may vary.

1. Create an project with an Postgres database. Then navigate to **Settings** -> **Tokens** to generate an project token.
Keep the token pinned in your system's clipboard or handy somewhere in your Bitwarden vault.
2. [Open this repository in Gitpod.](https://gitpod.io/#https://gitlab.com/RecapTime/infra/wiki.js) Once the workspace is fully booted up, create an `.railway-project-deploy.key` file and paste the deploy token you copied eariler.
3. In an terminal, run `direnv reload` to reload your shell with latest changes. Run `railway up` to deploy.
4. Wait for Wiki.js to boot up, complete setup and profit!

### With Divio

This template is formerly used to deploy in Divio, but due to cryptomining $#!tlords, make sure you have your credit card handy for humanity verification.

1. [**Sign in to Divio App**](https://control.divio.com) Prefer not to enter passwords? Use your GitHub (or Google) account to sign in.
For new accounts, follow prompts.
2. **Create a new project.** Follow the instructions below to set things up.
    - Hover to the plus icon on the left sidebar of your dashboard and click `New Project`.
    - **Give it a name.** Leave the organization option to its default settings.
    - **Hit Continue and choose a plan.** Leave it to the defaults if you don't want to scale up. Hit `Subscribe` to confirm.
3. In your newly-created app, click Services -> Add Service. Ensure that the service you want to provision is PostgresDB one. Leave the defaults as is and hit `Add New Service`.
Repeat the steps for the object storage.
4. Add your SSH keys first [in this page](https://control.divio.com/account/ssh-keys/) so you can clone
your Divio-hosted Git repositories through SSH.
5. Navigate to the Repository page and press `Migrate to external repository`. Paste the SSH url of your fork, copy Divio-provisioned public SSH key as [your fork's deploy key](https://docs.gitlab.com/ee/user/project/deploy_keys/#project-deploy-keys). Ensure that you tick the `Write access allowed` or Divio may complain about that.
6. [Configure webhooks](https://docs.divio.com/en/latest/how-to/resources-configure-git/#git-setup-webhook) for your Divio app so Divio can pull new commits automatically into your app.
7. Go back to the `Environments` screen, and under the `Live` environment, click **Deploy** button and confrim.
8. After successful deployment, visit your live deployment URL to complete Wiki.js setup. Profit!
